# Testing

Testing a (simple) Django API.

* Postman
* Unittest
    * Get
    * Post
    * Exceptions
* Pytest
    * Get
    * Post
    * Exceptions
    * Logs
